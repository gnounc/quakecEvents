/*
	to add a callback, write the function, call registerCallback() with the functin name and the event id,
	and add the function prototype to prototypes.qc, thats it!.


	to add an event, add a constant to the constants list in events.qc, update EVENT_COUNT with the latest event constant
	so it knows the upper bounds to initialize,  and add a call to checkCallbacks
	from where the event is triggered, passing the entity to check, and the event constant as parameters

	EVENT_KILLED is currently being called from killed(); for example


	for example and testing purposes, killTest and killTest2 callbacks are registered in the code. 
	killTest2 unregisters itself after it fires.

	Have fun! and please report any bugs : )
*/


